#include "Lista.h"

Lista::Lista() {
    _longitud = 0;
    _primero = NULL;
    _ultimo = NULL;
}

Lista::Nodo::Nodo(const int &v, const int &i, Lista::Nodo *ant, Lista::Nodo *sig):valor(v), indice(i), anterior(ant), siguiente(sig) {};

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.

    *this = l;
}

Lista::~Lista() {
    Nodo* temp = _primero;
    while (temp != NULL) {
        temp = temp->siguiente;
        delete _primero;
        _primero = temp;
    }
}

Lista& Lista::operator=(const Lista& aCopiar) {

    Nodo* nodoIndice = aCopiar._primero;
    for (int i=0; i<aCopiar._longitud; i++) {
        agregarAtras(nodoIndice->valor);
        nodoIndice = nodoIndice->siguiente;
    }

    _longitud = aCopiar._longitud;
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* nuevo = new Nodo(elem, -1, NULL, _primero);

    if (_longitud == 0) {
        _primero = nuevo;
        _ultimo = nuevo;
    }

    _longitud++;
    _primero->anterior = nuevo;
    _primero = nuevo;

    Nodo* nodoIndice = _primero;
    for (int i=0; i<_longitud; i++) {
        nodoIndice->indice++;
        nodoIndice = nodoIndice->siguiente;
    }

    _ultimo->siguiente = NULL;
    _primero->anterior = NULL;

}

void Lista::agregarAtras(const int& elem) {
    Nodo* nuevo = new Nodo(elem, _longitud, _ultimo, NULL);

    if (_longitud == 0) {
        _primero = nuevo;
        _ultimo = nuevo;
    }

    _longitud++;
    _ultimo->siguiente = nuevo;
    _ultimo = nuevo;

    _ultimo->siguiente = NULL;
    _primero->anterior = NULL;
}

void Lista::eliminar(Nat i) {

    if (i==0) {
        Nodo* nodoIndice = _primero->siguiente;
        _longitud--;
        for (int j=1; j<_longitud; j++) {
            nodoIndice->valor = nodoIndice->siguiente->valor;
            nodoIndice = nodoIndice->siguiente;
        }

    } else {
        Nodo* nodoIndice = _primero;
        for (int j=0; j<_longitud; j++) {
            if (nodoIndice->indice == i) {
                _longitud--;
                for (int h=i; h<_longitud; h++) {
                    nodoIndice->valor = nodoIndice->siguiente->valor;
                    nodoIndice = nodoIndice->siguiente;
                }
            } else {
                nodoIndice = nodoIndice->siguiente;
            }
        }
    }

}

int Lista::longitud() const {
    return _longitud;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* nodoIndice = _primero;
    for (int j=0; j<_longitud; j++) {
        if (nodoIndice->indice == i) {
            return nodoIndice->valor;
        } else {
            nodoIndice = nodoIndice->siguiente;
        }
    }
}

int& Lista::iesimo(Nat i) {
    Nodo* nodoIndice = _primero;
    for (int j=0; j<_longitud; j++) {
        if (nodoIndice->indice == i) {
            return nodoIndice->valor;
        } else {
            nodoIndice = nodoIndice->siguiente;
        }
    }
}

void Lista::mostrar(ostream& o) {
    // Completar
}

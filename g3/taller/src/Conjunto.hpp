
template <class T>
Conjunto<T>::Conjunto() {
    _cardinal = 0;
    _raiz = NULL;
}

template <class T>
Conjunto<T>::~Conjunto() { 
    destruir(_raiz);
}

template <class T>
void Conjunto<T>::destruir(Nodo* n) {
    if (n != NULL) {
        destruir(n->izq);
        destruir(n->der);
        delete n;
    }
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {

    if (_cardinal == 0) {
        return false;
    }

    bool res = false;


    Nodo* actual = _raiz;

    while (actual != NULL) {

        if (actual->valor == clave) {
            return true;
        }

        if (actual->valor > clave) {
            actual = actual->izq;
        } else {
            actual = actual->der;
        }
    }

    return res;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {

    if (!(pertenece(clave))) {

        if (_cardinal == 0) {
            Nodo* nuevo = new Nodo(clave);
            _raiz = nuevo;
        } else {
            Nodo* nuevo = new Nodo(clave);

            Nodo* actual = _raiz;

            while (actual->izq != nuevo && actual->der != nuevo) {
                if (actual->valor > clave) {
                    if (actual->izq == NULL) {
                        actual->izq = nuevo;
                    } else {
                        actual = actual->izq;
                    }
                } else {
                    if (actual->der == NULL) {
                        actual->der = nuevo;
                    } else {
                        actual = actual->der;
                    }
                }
            }

        }

        _cardinal++;

    }


}

template <class T>
void Conjunto<T>::remover(const T& clave) {

    _cardinal--;
    Nodo* padre = NULL;
    Nodo* hayQueBorrarlo = _raiz;
    bool borrarHijoIzq = false;
    bool borrarLaRaiz = true;

    while (hayQueBorrarlo->valor != clave) {
        padre = hayQueBorrarlo;
        if (hayQueBorrarlo->valor > clave) {
            hayQueBorrarlo = hayQueBorrarlo->izq;
            borrarHijoIzq = true;
        } else {
            hayQueBorrarlo = hayQueBorrarlo->der;
            borrarHijoIzq = false;
        }
    }

    if (padre != NULL) {
        borrarLaRaiz = false;
    }

    int cantHijos = 0;
    bool tieneHijoIzq = false;
    bool tieneHijoDer = false;


    if (hayQueBorrarlo->izq != NULL) {
        cantHijos++;
        tieneHijoIzq = true;
    }
    if (hayQueBorrarlo->der != NULL) {
        cantHijos++;
        tieneHijoDer = true;
    }

    if (cantHijos == 0) {
        if (borrarLaRaiz) {
            delete _raiz;
            _raiz = NULL;
        } else {
            if (borrarHijoIzq) {
                padre->izq = NULL;
            } else {
                padre->der = NULL;
            }
            delete hayQueBorrarlo;
        }
    }

    if (cantHijos == 1) {
        if (tieneHijoIzq) {
            if (borrarLaRaiz) {
                Nodo* temp = _raiz->izq;
                delete _raiz;
                _raiz = temp;
            } else {
                Nodo* temp = hayQueBorrarlo->izq;
                delete hayQueBorrarlo;
                if (borrarHijoIzq) {
                    padre->izq = temp;
                } else {
                    padre->der = temp;
                }
            }
        } else if (tieneHijoDer) {
            if (borrarLaRaiz) {
                Nodo* temp = _raiz->der;
                delete _raiz;
                _raiz = temp;
            } else {
                Nodo* temp = hayQueBorrarlo->der;
                delete hayQueBorrarlo;
                if (borrarHijoIzq) {
                    padre->izq = temp;
                } else {
                    padre->der = temp;
                }
            }
        }
    }

    if (cantHijos == 2) {
        if (borrarLaRaiz) {
            T sig = siguiente(_raiz->valor);
            remover(sig);
            _cardinal++;
            _raiz->valor = sig;
        } else {
            T sig = siguiente(hayQueBorrarlo->valor);
            remover(sig);
            _cardinal++;
            hayQueBorrarlo->valor = sig;
        }
    }

}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {

    stack<Nodo *> s;
    Nodo *actual = _raiz;

    int i=0;

    while (actual != NULL || s.empty() == false) {

        while (actual != NULL) {
            s.push(actual);
            actual = actual->izq;
        }
        actual = s.top();
        s.pop();
        if (i == 1) {
            return actual->valor;
        }
        if (actual->valor == clave) {
            i++;
        }
        actual = actual->der;
    }

}

template <class T>
const T& Conjunto<T>::minimo() const {

    Nodo* actual = _raiz;

    while (actual->izq != NULL) {
        actual = actual->izq;
    }

    return actual->valor;


}

template <class T>
const T& Conjunto<T>::maximo() const {

    Nodo* actual = _raiz;

    while (actual->der != NULL) {
        actual = actual->der;
    }

    return actual->valor;

}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}


#include "string_map.h"
#include <map>

template <typename T>
string_map<T>::string_map(){
    raiz = NULL;
    _size = 0;
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    vector<pair<string,T*>> v1 = claves();
    for (int i=0; i<v1.size(); i++) {
        erase(v1[i].first);
    }

    vector<pair<string,T*>> v2 = d.claves();
    for (int i=0; i<v2.size(); i++) {
        insert(make_pair(v2[i].first, *v2[i].second));
    }
}

template <typename T>
string_map<T>::~string_map(){
    vector<pair<string,T*>> v = claves();
    for (int i=0; i<v.size(); i++) {
        erase(v[i].first);
    }
}

template<typename T>
void string_map<T>::insert(const pair<string, T>& par) {
    if (raiz == NULL) {
        Nodo* nuevo = new Nodo();
        raiz = nuevo;
    }

    Nodo* actual = raiz;
    for (int i=0; i<par.first.size(); i++) {
        if (actual->siguientes[int(par.first.at(i))] == NULL) {
            Nodo* nuevo = new Nodo();
            actual->siguientes[int(par.first.at(i))] = nuevo;
            actual = nuevo;
        } else {
            actual = actual->siguientes[int(par.first.at(i))];
        }
    }

    if (actual->definicion != NULL) {
        delete actual->definicion;
        actual->definicion = NULL;
    }

    T* puntero = new T(par.second);
    actual->definicion = puntero;

    _size++;
}

template <typename T>
int string_map<T>::count(const string& clave) const{
    Nodo* actual = raiz;

    if (raiz == NULL) {
        return 0;
    }

    for (int i=0; i<clave.size(); i++) {
        if (actual->siguientes[int(clave.at(i))] == NULL) {
            return 0;
        } else {
            actual = actual->siguientes[int(clave.at(i))];
        }
    }

    if (actual->definicion == NULL) {
        return 0;
    } else {
        return 1;
    }
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    Nodo* actual = raiz;

    for (int i=0; i<clave.size(); i++) {
        actual = actual->siguientes[int(clave.at(i))];
    }

    T* res = actual->definicion;
    return *res;
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo* actual = raiz;

    for (int i=0; i<clave.size(); i++) {
        actual = actual->siguientes[int(clave.at(i))];
    }

    T* res = actual->definicion;
    return *res;
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    raiz = borrar(raiz, clave, 0);
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    return _size == 0;
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    // COMPLETAR
}

template<typename T>
vector<pair<string,T*>> string_map<T>::claves() const {
    return clavesConPrefijo("");
}

template<typename T>
vector<pair<string,T*>> string_map<T>::clavesConPrefijo(string pre) const {
    vector<pair<string,T*>>* q = new vector<pair<string,T*>>();
    collect(get(raiz, pre, 0), pre, q);
    vector<pair<string,T*>> res = *q;
    delete q;
    q = NULL;
    return res;
}

template<typename T>
string_map<T>::Nodo::Nodo() {
    siguientes = vector<Nodo*>(256, NULL);
    definicion = NULL;
}

template<typename T>
void string_map<T>::collect(string_map::Nodo* x, string pre, vector<pair<string,T*>>* q) const {
    if (x != NULL) {
        if (x->definicion != NULL) {
            q->push_back(make_pair(pre,x->definicion));
        }
        for (int i=0; i<255; i++) {
            char c = static_cast<char>(i);
            collect(x->siguientes[i], pre + c, q);
        }
    }
}

template<typename T>
typename string_map<T>::Nodo *string_map<T>::get(string_map::Nodo* x, string clave, int d) const {
    if (x == NULL) {
        return nullptr;
    }

    if (d == clave.length()) {
        return x;
    }

    char c = clave.at(d);
    return get(x->siguientes[int(c)], clave, d+1);
}

template<typename T>
typename string_map<T>::Nodo *string_map<T>::borrar(string_map::Nodo *x, string clave, int d) {
    if (x == NULL) {
        return nullptr;
    }

    bool esUltimo = true;

    if (d == clave.size()) {
        delete x->definicion;
        x->definicion = NULL;

        for (int i=0; i<255; i++) {
            if (x->siguientes[i] != NULL) {
                esUltimo = false;
            }
        }

    } else {
        char c = clave.at(d);
        x->siguientes[int(c)] = borrar(x->siguientes[int(c)], clave, d+1);
    }

    if (x->definicion != NULL) {
        return x;
    }

    for (int i=0; i<255; i++) {
        if (x->siguientes[i] != NULL) {
            return x;
        }
    }

    if (esUltimo) {
        delete x;
    }

    return nullptr;
}
#include <iostream>
#include <list>
#include <vector>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    bool operator==(Fecha o);
    void incrementar_dia();

  private:
    int mes_;
    int dia_;
};

Fecha::Fecha(int mes, int dia): mes_(mes), dia_(dia) {}

int Fecha::mes() {
    return mes_;
}

int Fecha::dia() {
    return dia_;
}

void Fecha::incrementar_dia() {
    uint d = dias_en_mes(mes_);
    if (dia_<d) {
        dia_++;
    } else {
        dia_=1;
        mes_++;
    }
}

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}

bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    return igual_dia && igual_mes;
}

// Ejercicio 11, 12

class Horario {
    public:
        Horario(uint hora, uint min);
        uint hora();
        uint min();
        bool operator==(Horario h);
        bool operator<(Horario h);
    private:
        uint hora_;
        uint min_;
};

Horario::Horario(uint hora, uint min): hora_(hora), min_(min) {}

uint Horario::hora() {
    return hora_;
}

uint Horario::min() {
    return min_;
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator==(Horario h) {
    bool igual_hora = this->hora() == h.hora();
    bool igual_min = this->min() == h.min();
    return igual_hora && igual_min;
}

bool Horario::operator<(Horario h) {
    if (this->hora() < h.hora()) {
        return true;
    } else if (this->hora() > h.hora()) {
        return false;
    } else {
        return (this->min() < h.min());
    }
}

// Ejercicio 13

class Recordatorio {
    public:
        Recordatorio(Fecha f, Horario h, string mensaje);
        Fecha fecha();
        Horario horario();
        string mensaje();
    private:
        Fecha fecha_;
        Horario horario_;
    string mensaje_;
};

Recordatorio::Recordatorio(Fecha f, Horario h, string mensaje): fecha_(f), horario_(h), mensaje_(mensaje) {}

Fecha Recordatorio::fecha() {
    return fecha_;
}

Horario Recordatorio::horario() {
    return horario_;
}

string Recordatorio::mensaje() {
    return mensaje_;
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.mensaje() << " @ " << r.fecha() << " " << r.horario();
    return os;
}

// Ejercicio 14

class Agenda {
    public:
        Agenda(Fecha fecha_inicial);
        void agregar_recordatorio(Recordatorio rec);
        void incrementar_dia();
        list<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();
    private:
        Fecha hoy_;
        map<tuple<int,int>, list<Recordatorio>> recordatorios_;
};

Agenda::Agenda(Fecha fecha_inicial): hoy_(fecha_inicial) {}

Fecha Agenda::hoy() {
    return hoy_;
}

void Agenda::incrementar_dia() {
    hoy_.incrementar_dia();
}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    tuple<int,int> fecha = {rec.fecha().dia(), rec.fecha().mes()};
    recordatorios_[fecha].push_back(rec);
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    int dia_de_hoy = hoy().dia();
    int mes_actual = hoy().mes();
    tuple<int,int> hoy = {dia_de_hoy, mes_actual};
    return recordatorios_[hoy];
}

ostream& operator<<(ostream& os, Agenda a) {
    os << a.hoy() << endl << "=====" << endl;
    vector<Recordatorio> bucket(1440, Recordatorio(Fecha(0, 0), Horario(0, 0), ""));
    for (Recordatorio r : a.recordatorios_de_hoy()) {
        bucket[r.horario().hora()*60 + r.horario().min()] = r;
    }
    for (int i=0; i<bucket.size(); i++) {
        if (bucket[i].mensaje() != "") {
            os << bucket[i].mensaje() << " @ " << bucket[i].fecha() << " " << bucket[i].horario() << endl;
        }
    }
    return os;
}